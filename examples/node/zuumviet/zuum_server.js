/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

var PROTO_PATH = __dirname + '/../../protos/note.proto';

var grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var note = grpc.loadPackageDefinition(packageDefinition).note;

// mysql
var mysql = require('mysql');


function listMessage(call, callback) {
  var connection = mysql.createConnection({
  host     : '124.158.12.85',
  user     : 'zuum_dev',
  password : 'ZRNfgbJ6rq',
  database : 'zuum_dev'
});

  connection.connect();


  let queries = `
      select m.id, m.title, m.short_content, IF(IsNull(ms.status), "Unread", ms.status) as status, m.image_url
      from messages m left join messages_user_list ms on m.id = ms.message_id and ms.partner_id = ` + call.request.partner_id + `
      where (ms.status is null or ms.status != "Deleted") and m.del_flg = 0 and m.message_type in (1,2) and DATE_SUB(curdate(), INTERVAL 6 MONTH) <= m.created_at
      and ((m.is_all_user = 1 and (ms.is_user_message != 1 || ms.is_user_message is null)) or (m.is_all_user != 1 and ms.is_user_message = 1 ))
      order by m.created_at desc
    `;

  connection.query(queries, function (error, results, fields) {
    if (error) {
      console.log(error);
    }
    callback(null, {items: results});
  });
  connection.end();

}





/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */
function main() {
  var server = new grpc.Server();
  server.addService(note.Greeter.service, {listMessage: listMessage});
  server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  console.log('connect');
  server.start();
}

main();
